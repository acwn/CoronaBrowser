/*

Filename: tabs.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#include <stdlib.h>

#include "tabs.h"
#include "helper.h"
#include "popup.h"


static void on_entry_activate (GtkEntry *entry, gpointer user_data)
{
	t_gb_tab *gb_tab = (t_gb_tab*) user_data;
	const gchar *uri = gtk_entry_get_text (entry);
  GtkTreeIter iter;

	if (!g_regex_match_simple ("^https?://", uri, G_REGEX_CASELESS, G_REGEX_MATCH_NOTEMPTY))
	{
		gchar *uri2 = g_strconcat ("https://", uri, NULL);
		webkit_web_view_load_uri (WEBKIT_WEB_VIEW (gb_tab->webview), uri2);
		gtk_list_store_append (gb_tab->entryliststore, &iter);
    gtk_list_store_set (gb_tab->entryliststore, &iter, 0, uri2, -1);
		g_free (uri2);
	}
	else
	{
		webkit_web_view_load_uri (WEBKIT_WEB_VIEW (gb_tab->webview), uri);
		gtk_list_store_append (gb_tab->entryliststore, &iter);
    gtk_list_store_set (gb_tab->entryliststore, &iter, 0, uri, -1);
	}
}

void on_entry_iconpress (GtkEntry *entry, GtkEntryIconPosition icon_pos, GdkEvent *event, gpointer user_data)
{
	t_gb_tab *gb_tab = (t_gb_tab*) user_data;
	GdkRectangle icon_area;

	switch (icon_pos)
	{
		case GTK_ENTRY_ICON_PRIMARY:
			if (event->type == GDK_BUTTON_PRESS)
				if (event->button.button == 1)
				{
					gtk_entry_get_icon_area (entry,GTK_ENTRY_ICON_PRIMARY, &icon_area);
					gb_create_primary_popup (gb_tab->toolbar_entry, &icon_area);
				}
		break;
		case GTK_ENTRY_ICON_SECONDARY:
			if (event->type == GDK_BUTTON_PRESS)
				if (event->button.button == 1)
				{
					gtk_entry_get_icon_area (entry,GTK_ENTRY_ICON_SECONDARY, &icon_area);
					gb_create_secondary_popup (gb_tab->toolbar_entry, &icon_area, webkit_web_view_get_title (WEBKIT_WEB_VIEW (gb_tab->webview)), webkit_web_view_get_uri (WEBKIT_WEB_VIEW ( gb_tab->webview)));
				}
		break;
	}
}

void on_entrycompletion_actionactivated (GtkEntryCompletion *widget, gint index, gpointer user_data)
{
	g_print("Test1: %d\n", index);
}

gboolean on_entrycompletion_matchselected (GtkEntryCompletion *widget, GtkTreeModel *model, GtkTreeIter *iter, gpointer user_data)
{
  t_gb_tab *gb_tab = (t_gb_tab*) user_data;
  gchar *uri;

  gtk_tree_model_get (model, iter, 0, &uri, -1);
  webkit_web_view_load_uri (WEBKIT_WEB_VIEW (gb_tab->webview), uri);
  gtk_entry_set_text (GTK_ENTRY (gb_tab->toolbar_entry), uri);
  g_free (uri);
  return TRUE;
}

static gboolean on_entrycompletion_match_function (GtkEntryCompletion *completion, const gchar *key, GtkTreeIter *iter, gpointer user_data)
{
  t_gb_tab *gb_tab = (t_gb_tab*) user_data;
  GtkTreeModel *model;
  gchar *uri;

  model = gtk_entry_completion_get_model (completion);
  if (G_UNLIKELY (model == NULL))
  {
    return FALSE;
  }
  gtk_tree_model_get (model, iter, 0, &uri, -1);
  if (uri == NULL)
  {
    return FALSE;
  }
  if (g_strstr_len (uri, -1, key) == NULL)
  {
    return FALSE;
  }
  else
  {
    return TRUE;
  }
}

void on_button_back_clicked (GtkButton *button, gpointer user_data)
{
	t_gb_tab *gb_tab = (t_gb_tab*) user_data;
	webkit_web_view_go_back (WEBKIT_WEB_VIEW (gb_tab->webview));
}

void on_button_forward_clicked (GtkButton *button, gpointer user_data)
{
	t_gb_tab *gb_tab = (t_gb_tab*) user_data;
	webkit_web_view_go_forward (WEBKIT_WEB_VIEW (gb_tab->webview));
}

void on_button_reload_clicked (GtkButton *button, gpointer user_data)
{
  t_gb_tab *gb_tab = (t_gb_tab*) user_data;

  if (webkit_web_view_is_loading (WEBKIT_WEB_VIEW (gb_tab->webview)))
  {
    webkit_web_view_stop_loading (WEBKIT_WEB_VIEW (gb_tab->webview));
  }
  else
  {
    webkit_web_view_reload (WEBKIT_WEB_VIEW (gb_tab->webview));
  }
}

void on_button_home_clicked (GtkButton *button, gpointer user_data)
{
	t_gb_tab *gb_tab = (t_gb_tab*) user_data;
	GSettings *settings = g_settings_new ("de.acwn.coronabrowser");
	webkit_web_view_load_uri (WEBKIT_WEB_VIEW (gb_tab->webview), g_settings_get_string (settings, "homepage"));
	g_clear_object (&settings);
}

static gboolean on_webview_contextmenu (WebKitWebView *web_view, WebKitContextMenu *context_menu, GdkEvent *event, WebKitHitTestResult *hit_test_result, gpointer user_data)
{
  GtkWidget *notebook = (GtkWidget*) user_data;
  return FALSE;
}

static gboolean on_webview_decidepolicy (WebKitWebView *web_view, WebKitPolicyDecision *decision, WebKitPolicyDecisionType decision_type, gpointer user_data)
{
  GtkWidget *notebook = (GtkWidget*) user_data;
  WebKitNavigationPolicyDecision *navigation_decision;
  WebKitNavigationAction *navigation_action;
  WebKitURIRequest *uri_request;
  const gchar *url;
  WebKitResponsePolicyDecision *response;

  switch (decision_type)
  {
    case WEBKIT_POLICY_DECISION_TYPE_NAVIGATION_ACTION:
      navigation_decision = WEBKIT_NAVIGATION_POLICY_DECISION (decision);
    break;
    case WEBKIT_POLICY_DECISION_TYPE_NEW_WINDOW_ACTION:
      navigation_decision = WEBKIT_NAVIGATION_POLICY_DECISION (decision);
      navigation_action = webkit_navigation_policy_decision_get_navigation_action (navigation_decision);
      uri_request = webkit_navigation_action_get_request (navigation_action);
      url = webkit_uri_request_get_uri (uri_request);
      gb_new_tab (notebook, url, FALSE);
    break;
    case WEBKIT_POLICY_DECISION_TYPE_RESPONSE:
      response = WEBKIT_RESPONSE_POLICY_DECISION (decision);
      uri_request = webkit_response_policy_decision_get_request (response);
      url = webkit_uri_request_get_uri (uri_request);
      //      g_print("%s\n", url);
    break;
    default:
      return FALSE;
    break;
  }
  return TRUE;
}

static void on_webview_loadchanged (WebKitWebView *web_view, WebKitLoadEvent load_event, gpointer user_data)
{
  t_gb_tab *gb_tab = (t_gb_tab*) user_data;
  GFile *file;
  GIcon *icon;

  switch (load_event)
  {
    case WEBKIT_LOAD_STARTED:
      gtk_entry_set_text (GTK_ENTRY (gb_tab->toolbar_entry), webkit_web_view_get_uri (WEBKIT_WEB_VIEW (gb_tab->webview)));
      file = g_file_new_for_path (get_full_data_path ("lock-grey.svg"));
      icon = g_file_icon_new (file);
      gtk_entry_set_icon_from_gicon (GTK_ENTRY (gb_tab->toolbar_entry), GTK_ENTRY_ICON_PRIMARY, icon);
      gtk_tool_button_set_icon_name (GTK_TOOL_BUTTON (gb_tab->toolbar_reload), "window-close");
    break;
    case WEBKIT_LOAD_REDIRECTED:
      gtk_entry_set_text (GTK_ENTRY (gb_tab->toolbar_entry), webkit_web_view_get_uri (WEBKIT_WEB_VIEW (gb_tab->webview)));
    break;
    case WEBKIT_LOAD_COMMITTED:
      gtk_entry_set_text (GTK_ENTRY (gb_tab->toolbar_entry), webkit_web_view_get_uri (WEBKIT_WEB_VIEW (gb_tab->webview)));
    break;
    case WEBKIT_LOAD_FINISHED:
      gtk_entry_set_text (GTK_ENTRY (gb_tab->toolbar_entry), webkit_web_view_get_uri (WEBKIT_WEB_VIEW (gb_tab->webview)));
      gtk_label_set_text (GTK_LABEL (gb_tab->label), webkit_web_view_get_title (WEBKIT_WEB_VIEW (gb_tab->webview)));
      gtk_widget_set_tooltip_text (GTK_WIDGET (gb_tab->label), webkit_web_view_get_title (WEBKIT_WEB_VIEW (gb_tab->webview)));
      gtk_tool_button_set_icon_name (GTK_TOOL_BUTTON (gb_tab->toolbar_reload), "view-refresh");
      gtk_widget_set_sensitive (GTK_WIDGET (gb_tab->toolbar_back), webkit_web_view_can_go_back (WEBKIT_WEB_VIEW (gb_tab->webview)));
      gtk_widget_set_sensitive (GTK_WIDGET (gb_tab->toolbar_forward), webkit_web_view_can_go_forward (WEBKIT_WEB_VIEW (gb_tab->webview)));
      if (!gb_tab->insecurecontent)
      {
        file = g_file_new_for_path (get_full_data_path ("lock-green.svg"));
        icon = g_file_icon_new (file);
        gtk_entry_set_icon_from_gicon (GTK_ENTRY (gb_tab->toolbar_entry), GTK_ENTRY_ICON_PRIMARY, icon);
      }
      else
      {
	gb_tab->insecurecontent = FALSE;
      }
    break;
  }
}

static gboolean on_webview_loadfailedwithtlserrors (WebKitWebView *web_view, gchar *failing_uri, GTlsCertificate *certificate, GTlsCertificateFlags errors, gpointer user_data)
{
  g_print("loadfailedtlserror: %s\n", failing_uri);
  return TRUE;
}

static gboolean on_webview_permissionrequest (WebKitWebView *web_view, WebKitPermissionRequest *request, gpointer user_data)
{
  g_print("request");
  return TRUE;
}

static void on_webview_insecurecontentdetected (WebKitWebView *web_view, WebKitInsecureContentEvent event, gpointer user_data)
{
  GFile *file;
  GIcon *icon;

  t_gb_tab *gb_tab = (t_gb_tab*) user_data;
  gb_tab->insecurecontent = TRUE;
  file = g_file_new_for_path (get_full_data_path ("lock-yellow.svg"));
  icon = g_file_icon_new (file);
  gtk_entry_set_icon_from_gicon (GTK_ENTRY (gb_tab->toolbar_entry), GTK_ENTRY_ICON_PRIMARY, icon);
}

static void on_webview_mousetargetchanged (WebKitWebView *web_view, WebKitHitTestResult *hit_test_result, guint modifiers, gpointer user_data)
{
  t_gb_tab *gb_tab = (t_gb_tab*) user_data;

  gtk_label_set_label (GTK_LABEL (gb_tab->actionbar_label), webkit_hit_test_result_get_link_uri (hit_test_result));
}

static void on_webview_estimatedloadprogress (WebKitWebView *web_view, GParamSpec *pspec, gpointer user_data)
{
  t_gb_tab *gb_tab = (t_gb_tab*) user_data;

  gtk_entry_set_progress_fraction (GTK_ENTRY (gb_tab->toolbar_entry), webkit_web_view_get_estimated_load_progress (web_view));
}

static void on_webview_submitform (WebKitWebView *web_view, WebKitFormSubmissionRequest *request, gpointer user_data)
{
  t_gb_tab *gb_tab = (t_gb_tab*) user_data;
  g_print("submitform\n");
  GPtrArray *field_names;
  GPtrArray *field_values;
  
//  webkit_form_submission_request_list_text_fields (request, &field_names, &field_values);

/*  
  for (gsize i = 0; i < field_names->len; i++)
  {
    const gchar *element = g_ptr_array_index (field_names, i);
    g_print ("%s\n", element);
    const gchar *element2 = g_ptr_array_index (field_values, i);
    g_print ("%s\n", element2);
  }
*/
//  g_ptr_array_free (field_names, TRUE);
//  g_ptr_array_free (field_values, TRUE);
}

void gb_new_tab (GtkWidget *notebook, const gchar *url, gboolean private)
{
  t_gb_tab *gb_tab = malloc (sizeof (t_gb_tab));	
  gb_tab->insecurecontent = FALSE;
  gb_tab->bookmark = g_bookmark_file_new ();

  /* tab label */
  gb_tab->box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 1);
  gb_tab->image = gtk_image_new_from_icon_name ("document-new", GTK_ICON_SIZE_BUTTON);
  gb_tab->label = gtk_label_new ("New page");
  gtk_widget_set_hexpand (GTK_WIDGET (gb_tab->label), TRUE);
  gtk_label_set_ellipsize (GTK_LABEL (gb_tab->label), PANGO_ELLIPSIZE_END);
  gtk_box_pack_start (GTK_BOX (gb_tab->box), gb_tab->image, FALSE, FALSE, 0);
  gtk_box_pack_start (GTK_BOX (gb_tab->box), gb_tab->label, TRUE, TRUE, 0);
  gtk_widget_show_all (GTK_WIDGET (gb_tab->box));

  /* content */
  gb_tab->grid = gtk_grid_new ();
  gb_tab->toolbar = gtk_toolbar_new ();
  gb_tab->toolbar_back = gtk_tool_button_new (NULL, "Back");
  gtk_tool_button_set_icon_name (GTK_TOOL_BUTTON (gb_tab->toolbar_back), "go-previous");
  gb_tab->toolbar_forward = gtk_tool_button_new (NULL, "Forward");
  gtk_tool_button_set_icon_name (GTK_TOOL_BUTTON (gb_tab->toolbar_forward), "go-next");
  gb_tab->toolbar_reload = gtk_tool_button_new (NULL, "Reload");
  gtk_tool_button_set_icon_name (GTK_TOOL_BUTTON (gb_tab->toolbar_reload), "window-close");
  gb_tab->toolbar_home = gtk_tool_button_new (NULL, "Home");
  gtk_tool_button_set_icon_name (GTK_TOOL_BUTTON (gb_tab->toolbar_home), "go-home");

  gb_tab->toolbar_container_entry = gtk_tool_item_new ();
  gb_tab->toolbar_entry = gtk_entry_new ();
  gtk_container_add (GTK_CONTAINER(gb_tab->toolbar_container_entry), GTK_WIDGET(gb_tab->toolbar_entry));
  gtk_tool_item_set_expand (GTK_TOOL_ITEM (gb_tab->toolbar_container_entry), TRUE);
  gtk_entry_set_icon_from_icon_name (GTK_ENTRY (gb_tab->toolbar_entry), GTK_ENTRY_ICON_SECONDARY, "bookmark-new");
  gtk_entry_set_icon_tooltip_text (GTK_ENTRY (gb_tab->toolbar_entry), GTK_ENTRY_ICON_SECONDARY, "Bookmark it");
  gtk_entry_set_input_purpose (GTK_ENTRY (gb_tab->toolbar_entry), GTK_INPUT_PURPOSE_URL);
  gtk_entry_set_text (GTK_ENTRY (gb_tab->toolbar_entry), url);

  gtk_toolbar_insert (GTK_TOOLBAR (gb_tab->toolbar), GTK_TOOL_ITEM (gb_tab->toolbar_back), -1);
  gtk_toolbar_insert (GTK_TOOLBAR (gb_tab->toolbar), GTK_TOOL_ITEM (gb_tab->toolbar_forward), -1);
  gtk_toolbar_insert (GTK_TOOLBAR (gb_tab->toolbar), GTK_TOOL_ITEM (gb_tab->toolbar_reload), -1);
  gtk_toolbar_insert (GTK_TOOLBAR (gb_tab->toolbar), GTK_TOOL_ITEM (gb_tab->toolbar_home), -1);
  gtk_toolbar_insert (GTK_TOOLBAR (gb_tab->toolbar), GTK_TOOL_ITEM (gb_tab->toolbar_container_entry), -1);

  gb_tab->entrycompletion = gtk_entry_completion_new ();
  gb_tab->entryliststore = gtk_list_store_new (1, G_TYPE_STRING);
  gtk_entry_completion_set_model (gb_tab->entrycompletion, GTK_TREE_MODEL (gb_tab->entryliststore));
  gtk_entry_completion_set_text_column (gb_tab->entrycompletion, 0);
  gtk_entry_completion_set_popup_completion (gb_tab->entrycompletion, TRUE);
  gtk_entry_completion_set_match_func (gb_tab->entrycompletion, on_entrycompletion_match_function, gb_tab, NULL);
  gtk_entry_set_completion (GTK_ENTRY (gb_tab->toolbar_entry), gb_tab->entrycompletion);

  if (private)
  {
    gb_tab->context = webkit_web_context_new_ephemeral ();
  }
  else
  {
    gb_tab->context = webkit_web_context_new ();
  }
  webkit_web_context_set_process_model (gb_tab->context, WEBKIT_PROCESS_MODEL_MULTIPLE_SECONDARY_PROCESSES);
  gb_tab->webview = webkit_web_view_new_with_context (gb_tab->context);

  gb_tab->settings = webkit_web_view_get_settings (WEBKIT_WEB_VIEW (gb_tab->webview));
  webkit_settings_set_user_agent (gb_tab->settings, build_agent_string (webkit_settings_get_user_agent (gb_tab->settings)));
  webkit_web_view_set_settings (WEBKIT_WEB_VIEW (gb_tab->webview), gb_tab->settings);
  webkit_web_view_load_uri (WEBKIT_WEB_VIEW (gb_tab->webview), url);

  gtk_widget_set_hexpand (GTK_WIDGET (gb_tab->webview), TRUE);
  gtk_widget_set_vexpand (GTK_WIDGET (gb_tab->webview), TRUE);

  /* actionbar */
  gb_tab->actionbar = gtk_action_bar_new ();
  gb_tab->actionbar_label = gtk_label_new ("Hallo");
  gtk_label_set_ellipsize (GTK_LABEL (gb_tab->actionbar_label), PANGO_ELLIPSIZE_END);
  gtk_action_bar_pack_start (GTK_ACTION_BAR (gb_tab->actionbar), gb_tab->actionbar_label);

  /* build the build */
  gtk_grid_attach (GTK_GRID (gb_tab->grid), gb_tab->toolbar, 0, 0, 1, 1);
  gtk_grid_attach (GTK_GRID (gb_tab->grid), gb_tab->webview, 0, 1, 1, 1);
  gtk_grid_attach (GTK_GRID (gb_tab->grid), gb_tab->actionbar, 0, 2, 1, 1);
  gtk_widget_show_all (GTK_WIDGET (gb_tab->grid));
	
  /* Signals */
  g_signal_connect (gb_tab->toolbar_entry, "activate", G_CALLBACK (on_entry_activate), gb_tab);
  g_signal_connect (gb_tab->toolbar_entry, "icon-press", G_CALLBACK (on_entry_iconpress), gb_tab);

  g_signal_connect (gb_tab->entrycompletion, "action-activated", G_CALLBACK (on_entrycompletion_actionactivated), NULL);
  g_signal_connect (gb_tab->entrycompletion, "match-selected", G_CALLBACK (on_entrycompletion_matchselected), gb_tab);

  g_signal_connect (gb_tab->toolbar_back, "clicked", G_CALLBACK (on_button_back_clicked), gb_tab);
  g_signal_connect (gb_tab->toolbar_forward, "clicked", G_CALLBACK (on_button_forward_clicked), gb_tab);
  g_signal_connect (gb_tab->toolbar_reload, "clicked", G_CALLBACK (on_button_reload_clicked), gb_tab);
  g_signal_connect (gb_tab->toolbar_home, "clicked", G_CALLBACK (on_button_home_clicked), gb_tab);

  g_signal_connect (gb_tab->webview, "context-menu", G_CALLBACK (on_webview_contextmenu), notebook);
  g_signal_connect (gb_tab->webview, "decide-policy", G_CALLBACK (on_webview_decidepolicy), notebook);
  g_signal_connect (gb_tab->webview, "load-changed", G_CALLBACK (on_webview_loadchanged), gb_tab);
  g_signal_connect (gb_tab->webview, "load-failed-with-tls-errors", G_CALLBACK (on_webview_loadfailedwithtlserrors), gb_tab);
  g_signal_connect (gb_tab->webview, "permission-request", G_CALLBACK (on_webview_permissionrequest), gb_tab);
  g_signal_connect (gb_tab->webview, "insecure-content-detected", G_CALLBACK (on_webview_insecurecontentdetected), gb_tab);
  g_signal_connect (gb_tab->webview, "mouse-target-changed", G_CALLBACK (on_webview_mousetargetchanged), gb_tab);
  g_signal_connect (gb_tab->webview, "notify::estimated-load-progress", G_CALLBACK (on_webview_estimatedloadprogress), gb_tab);
  //  g_signal_connect (gb_tab->webview, "submit-form", G_CALLBACK (on_webview_submitform), gb_tab);

  gint index = gtk_notebook_append_page (GTK_NOTEBOOK (notebook), gb_tab->grid, gb_tab->box);
  gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook), index);
}

void gb_close_tab (GtkWidget *notebook)
{
  //  g_bookmark_file_free (gb_tab->bookmark);
  gtk_notebook_remove_page (GTK_NOTEBOOK(notebook), gtk_notebook_get_current_page GTK_NOTEBOOK(notebook));
}
