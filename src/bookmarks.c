/*

Filename: bookmarks.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/


#include "bookmarks.h"
#include "additem.h"
#include "bookmarks_backend.h"


G_DEFINE_TYPE (CoronabrowserBookmarks, coronabrowser_bookmarks, GTK_TYPE_DIALOG)


static void coronabrowser_bookmarks_init (CoronabrowserBookmarks *win)
{
  GtkTreeIter child;
  GtkTreeSelection *selection;
  t_backend *be = backend_new ();

  gtk_widget_init_template (GTK_WIDGET (win));
  gtk_tree_store_append (win->treestore1, &child, NULL);
  gtk_tree_store_set (win->treestore1, &child, 0, "Labels", -1);
  selection = gtk_tree_view_get_selection (win->treeview_folders);
  gtk_tree_selection_select_iter (selection, &child);
  backend_get_child_folders (be, win->treeview_folders, "Labels");
  backend_free (be);
}

static void coronabrowser_bookmarks_dispose (GObject *object)
{
  G_OBJECT_CLASS (coronabrowser_bookmarks_parent_class)->dispose (object);
}

static void on_okbutton_clicked (GtkWidget *widget, CoronabrowserBookmarks *win)
{
  gtk_window_close (GTK_WINDOW (win));
}

static void on_bookmark_new_clicked (GtkWidget *widget, CoronabrowserBookmarks *win)
{
  GtkTreeIter iter;
  
//  win = CORONABROWSER_BOOKMARKS (gtk_widget_get_toplevel (GTK_WIDGET (widget)));
//  gtk_list_store_append (win->liststore1, &iter);
//  gtk_list_store_set (win->liststore1, &iter, 0, "Checking", -1);
}

static void on_label_new_clicked (GtkWidget *widget, CoronabrowserBookmarks *win)
{
  CoronabrowserAdditem *dialog;
  const gchar *retval;
  const gchar *parent_folder;
  GtkTreeSelection *selection;
  GtkTreeModel *model;
  GtkTreeIter child;
  GtkTreeIter parent;

  dialog = coronabrowser_additem_new (CORONABROWSER_BOOKMARKS (win));
  switch (gtk_dialog_run (GTK_DIALOG (dialog)))
  {
    case GTK_RESPONSE_APPLY:
      retval = coronabrowser_additem_get_item (CORONABROWSER_ADDITEM (dialog));
      selection = gtk_tree_view_get_selection (win->treeview_folders);
      if (gtk_tree_selection_get_selected (selection, &model, &parent))
      {
        gtk_tree_store_append (win->treestore1, &child, &parent);
        gtk_tree_store_set (win->treestore1, &child, 0, retval, -1);
      }
      else
      {
        gtk_tree_store_append (win->treestore1, &child, NULL);
        gtk_tree_store_set (win->treestore1, &child, 0, retval, -1);
      }
      gtk_tree_model_get (model, &parent, 0, &parent_folder, -1);
      gtk_tree_selection_select_iter (selection, &child);
      t_backend *be = backend_new ();
      backend_add_label (be, retval);
      backend_free (be);
    break;
    case GTK_RESPONSE_CANCEL:
    break;
  }
}

static void on_bookmark_delete_clicked (GtkWidget *widget, CoronabrowserBookmarks *win)
{
  GtkTreeSelection *selection;
  GtkTreeModel *model;
  GtkTreeIter iter;
  t_bookmark *bookmark;

  selection = gtk_tree_view_get_selection (win->treeview_bookmarks);
  if (!gtk_tree_selection_get_selected (selection, &model, &iter))
    return;

  t_backend *be = backend_new ();
  bookmark = g_malloc0 (sizeof (t_bookmark));
  gtk_tree_model_get (model, &iter, 0, &bookmark->title, 1, &bookmark->address, -1);
  if (backend_delete_bookmark (be, bookmark))
  {
    gtk_list_store_remove (GTK_LIST_STORE (model), &iter);
    gtk_entry_set_text (GTK_ENTRY (win->bookmark_title), "");
    gtk_entry_set_text (GTK_ENTRY (win->bookmark_link), "");
  }
  else
  {
  }
  g_free (bookmark);
  backend_free (be);
}

static void on_label_delete_clicked (GtkWidget *widget, CoronabrowserBookmarks *win)
{
}

static void on_bookmark_title_activate (GtkWidget *widget, CoronabrowserBookmarks *win)
{
}

static void on_bookmark_link_activate (GtkWidget *widget, CoronabrowserBookmarks *win)
{
}

static void on_folders_selection_changed (GtkTreeSelection *treeselection, CoronabrowserBookmarks *win)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  t_backend *be = backend_new ();
  const gchar *folder;

  if (!gtk_tree_selection_get_selected (treeselection, &model, &iter))
    return;
  gtk_tree_model_get (model, &iter, 0, &folder, -1);
  gtk_list_store_clear (win->liststore1);
  backend_get_all_bookmarks (be, win->liststore1, folder);
//  backend_get_child_folders (be, win->treeview_folders, folder);
  backend_free (be);
}

static void on_bookmarks_selection_changed (GtkTreeSelection *treeselection, CoronabrowserBookmarks *win)
{
  GtkTreeModel *model;
  GtkTreeIter iter;
  const gchar *title;
  const gchar *address;

  if (!gtk_tree_selection_get_selected (treeselection, &model, &iter))
    return;
  gtk_tree_model_get (model, &iter, 0, &title, 1, &address, -1);
  gtk_entry_set_text (GTK_ENTRY (win->bookmark_title), title);
  gtk_entry_set_text (GTK_ENTRY (win->bookmark_link), address);
}

static void coronabrowser_bookmarks_class_init (CoronabrowserBookmarksClass *class)
{
  G_OBJECT_CLASS (class)->dispose = coronabrowser_bookmarks_dispose;
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class), "/de/acwn/coronabrowser/bookmarks.ui");

  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, okbutton);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, bookmark_new);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, label_new);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, bookmark_delete);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, label_delete);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, bookmark_title);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, bookmark_link);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, treeview_folders);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, treestore1);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, treeview_bookmarks);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserBookmarks, liststore1);

  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_okbutton_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_bookmark_new_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_label_new_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_bookmark_delete_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_label_delete_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_bookmark_title_activate);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_bookmark_link_activate);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_folders_selection_changed);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_bookmarks_selection_changed);
}


CoronabrowserBookmarks *coronabrowser_bookmarks_new (CoronabrowserWindow *win)
{
  return g_object_new (CORONABROWSER_BOOKMARKS_TYPE, "transient-for", win, NULL);
}

GtkWidget *coronabrowser_bookmarks_new_menu (void)
{
  GtkWidget *menu;
  GtkWidget *submenu;
  GtkWidget *test;

  menu = gtk_menu_new ();
  submenu = gtk_menu_new ();
  test = gtk_menu_item_new_with_label ("Test");
  gtk_menu_item_set_submenu (GTK_MENU_ITEM (test), submenu);
  
  gtk_menu_shell_append (GTK_MENU_SHELL (menu), test);
  gtk_widget_show (test);

  return menu;
}
