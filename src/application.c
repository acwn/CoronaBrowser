/*

Filename: application.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#include <glib.h>
#include <glib/gstdio.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "application.h"
#include "window.h"

static GOptionEntry app_options[] =
{
  { "version", 'v', G_OPTION_FLAG_NONE, G_OPTION_ARG_NONE, NULL, "Version", NULL },
  { "new-tab", 't', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, NULL, "Open <url> in a new tab", "<url>" },
  { "new-window", 'w', G_OPTION_FLAG_NONE, G_OPTION_ARG_STRING, NULL, "Open <url> in a new window", "<url>" },
  { NULL }
};

struct _CoronabrowserApp
{
  GtkApplication parent;
};

G_DEFINE_TYPE (CoronabrowserApp, coronabrowser_app, GTK_TYPE_APPLICATION);

static void coronabrowser_app_init (CoronabrowserApp *app)
{
  g_application_add_main_option_entries (G_APPLICATION (app), app_options);
  const gchar *homedir = g_get_home_dir ();
  const gchar *configdir = g_build_path ("/", homedir, ".config", PACKAGE_NAME, NULL);
  g_mkdir (configdir, 0755);
}

static void application_startup (GApplication *app)
{
  G_APPLICATION_CLASS (coronabrowser_app_parent_class)->startup (app);
}

static void application_activate (GApplication *app)
{
  CoronabrowserWindow *win;

  win = coronabrowser_window_new (CORONABROWSER_APP (app));
  gtk_window_present (GTK_WINDOW (win));
}

static gint application_commandline (GApplication *app, GApplicationCommandLine *command_line)
{
  GVariantDict *options_dict = g_application_command_line_get_options_dict (command_line);
  gchar *url_tab = NULL;
  gchar *url_window = NULL;
  CoronabrowserWindow *win;
  GList *windows;

  g_variant_dict_lookup (options_dict, "new-tab", "s", &url_tab);
  g_variant_dict_lookup (options_dict, "new-window", "s", &url_window);

  windows = gtk_application_get_windows (GTK_APPLICATION (app));
  if (windows)
    win = CORONABROWSER_WINDOW (windows->data);
  else
    win = coronabrowser_window_new (CORONABROWSER_APP (app));
  if (G_UNLIKELY (url_tab))
    coronabrowser_window_open (win, url_tab);
  if (G_UNLIKELY (url_window))
  {
    win = coronabrowser_window_new (CORONABROWSER_APP (app));
    coronabrowser_window_open (win, url_window);
  }
  gtk_window_present (GTK_WINDOW (win));
}

static gint application_handlelocaloptions (GApplication *app, GVariantDict *options)
{
  gboolean version = FALSE;

  g_variant_dict_lookup (options, "version", "b", &version);
  if (G_UNLIKELY (version))
  {
    g_print ("%s %s\n\n", PACKAGE_NAME, PACKAGE_VERSION);
    g_print ("%s\n\n", "Copyright (c) 2018 - 2021 Alexander Nagel");
    g_print ("Please report bugs to <%s>.\n", PACKAGE_BUGREPORT);
    return 0;
  }
  return -1;
}

static void application_finalize (GObject *object)
{
  G_OBJECT_CLASS (coronabrowser_app_parent_class)->finalize (object);
}

void coronabrowser_app_class_init (CoronabrowserAppClass *class)
{
  G_APPLICATION_CLASS (class)->startup = application_startup;
  G_APPLICATION_CLASS (class)->activate = application_activate;
  G_APPLICATION_CLASS (class)->command_line = application_commandline;
  G_APPLICATION_CLASS (class)->handle_local_options = application_handlelocaloptions;
}


CoronabrowserApp *coronabrowser_new (void)
{
  g_set_application_name (PACKAGE_NAME);
  return g_object_new (CORONABROWSER_APP_TYPE, "application-id", "de.acwn.coronabrowser",
    "flags", G_APPLICATION_HANDLES_COMMAND_LINE,
    NULL);
}
