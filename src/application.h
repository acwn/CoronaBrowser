/*

Filename: application.h
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#ifndef APPLICATION_H
#define APPLICATION_H

#include <gtk/gtk.h>

#define CORONABROWSER_APP_TYPE (coronabrowser_app_get_type ())
G_DECLARE_FINAL_TYPE (CoronabrowserApp, coronabrowser_app, CORONABROWSER, APP, GtkApplication)

CoronabrowserApp *coronabrowser_new (void);

#endif
