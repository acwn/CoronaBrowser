/*

Filename: tabs.h
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#ifndef __TABS_H__
#define __TABS_H__

#include <gtk/gtk.h>
#include <webkitgtk-4.0/webkit2/webkit2.h>

typedef struct s_gb_tab
{
  /* label */
  GtkWidget *box;
  GtkWidget *image;
  GtkWidget *label;

  /* child */
  GtkWidget *grid;

  /* toolbar and its buttons */
  GtkWidget *toolbar;
  GtkToolItem *toolbar_back;
  GtkToolItem *toolbar_forward;
  GtkToolItem *toolbar_reload;
  GtkToolItem *toolbar_home;
  GtkToolItem *toolbar_container_entry;
  GtkWidget *toolbar_entry;
  GtkEntryCompletion *entrycompletion;
  GtkListStore *entryliststore;

  /* webview */
  GtkWidget *webview;
  WebKitWebContext *context;
  WebKitSettings *settings;
  gboolean insecurecontent;

  /* actionbar and its buttons */
  GtkWidget *actionbar;
  GtkWidget *actionbar_label;

  /* other */
  GBookmarkFile *bookmark;
} t_gb_tab;

void gb_new_tab (GtkWidget *notebook, const gchar *url, gboolean private);

void gb_close_tab (GtkWidget *notebook);
#endif
