/*

Filename: preferences.h
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <gtk/gtk.h>
#include "window.h"

#define CORONABROWSER_PREFERENCES_TYPE (coronabrowser_preferences_get_type())
G_DECLARE_FINAL_TYPE (CoronabrowserPreferences, coronabrowser_preferences, CORONABROWSER, PREFERENCES, GtkDialog)

CoronabrowserPreferences *coronabrowser_preferences_new (CoronabrowserWindow *win);

#endif
