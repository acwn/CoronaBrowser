/*

Filename: bookmarks.h
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#ifndef BOOKMARKS_H
#define BOOKMARKS_H

#include <gtk/gtk.h>
#include "window.h"

struct _CoronabrowserBookmarks
{
  GtkDialog parent;

  GtkWidget *okbutton;
  GtkWidget *bookmark_new;
  GtkWidget *label_new;
  GtkWidget *bookmark_delete;
  GtkWidget *label_delete;
  GtkWidget *bookmark_title;
  GtkWidget *bookmark_link;
  GtkTreeView *treeview_folders;
  GtkTreeStore *treestore1;
  GtkTreeView *treeview_bookmarks;
  GtkListStore *liststore1;
};

#define CORONABROWSER_BOOKMARKS_TYPE (coronabrowser_bookmarks_get_type())

G_DECLARE_FINAL_TYPE (CoronabrowserBookmarks, coronabrowser_bookmarks, CORONABROWSER, BOOKMARKS, GtkDialog)

CoronabrowserBookmarks *coronabrowser_bookmarks_new (CoronabrowserWindow *win);

GtkWidget *coronabrowser_bookmarks_new_menu (void);

#endif
