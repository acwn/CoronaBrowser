/*

Filename: helper.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "helper.h"

void show_message_dialog (const gchar *message01, const gchar *message02)
{
  GtkWidget *dialog;

  GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT;
  dialog = gtk_message_dialog_new (NULL, flags, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "%s", message01);
  gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog), "%s", message02);
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}

gchar *build_agent_string (const gchar *defaultagent)
{
  return g_strconcat (defaultagent, " ", PACKAGE_NAME, "/", PACKAGE_VERSION, NULL);
}

gchar *get_full_data_path (const gchar *filename)
{
	gchar *fullname;
	fullname = g_build_filename (DATADIR, filename, NULL);
	if (g_file_test (fullname, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))
	{
		return fullname;
	}
	else
	{
		return NULL;
	}
}

void gb_show_about_dialog (CoronabrowserWindow *window)
{
  GdkPixbuf *pixbuf = NULL;
  GError *error = NULL;
  const gchar *authors[] = {"Alexander Nagel <alexander@acwn.de>", NULL};

  pixbuf = gdk_pixbuf_new_from_file (get_full_data_path ("coronabrowser-small.png"), &error);
  if (pixbuf == NULL)
  {
    show_message_dialog ("Can't open file", error->message);
  }

	gtk_show_about_dialog (GTK_WINDOW (window),
	"copyright", "Copyright (c) 2018 - 2020 Alexander Nagel",
	"license-type", GTK_LICENSE_GPL_3_0,
	"program-name", PACKAGE_NAME,
	"version", PACKAGE_VERSION,
	"website", "https://www.acwn.de/projects/coronabrowser/",
	"website-label", "Project page",
	"logo", pixbuf,
	"authors", authors,
	NULL 
	);
}
