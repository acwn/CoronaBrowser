/*

Filename: helper.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#ifndef HELPER_H
#define HELPER_H

#include <gtk/gtk.h>
#include <glib.h>

#include "window.h"

void show_message_dialog (const gchar *message01, const gchar *message02);
gchar *get_full_data_path (const gchar *filename);
void gb_show_about_dialog (CoronabrowserWindow *window);
gchar *build_agent_string (const gchar *defaultagent);

#endif
