/*

Filename: additem.c
Copyright © 2018 - 2021  Alexander Nagel
Email: coronabrowser@acwn.de
Homepage: http://www.acwn.de/projects/coronabrowser

This file is part of coronabrowser.

*/

#include "additem.h"

struct _CoronabrowserAdditem
{
  GtkDialog parent;

  GtkWidget *cancelbutton;
  GtkWidget *addbutton;
  GtkWidget *entryitem;
};

G_DEFINE_TYPE (CoronabrowserAdditem, coronabrowser_additem, GTK_TYPE_DIALOG)

static void coronabrowser_additem_init (CoronabrowserAdditem *win)
{
  gtk_widget_init_template (GTK_WIDGET (win));
}

static void coronabrowser_additem_dispose (GObject *object)
{
  CoronabrowserAdditem *win;

  win = CORONABROWSER_ADDITEM (object);
  G_OBJECT_CLASS (coronabrowser_additem_parent_class)->dispose (object);
}

static void on_cancelbutton_clicked (GtkWidget *widget, CoronabrowserAdditem *win)
{
  gtk_window_close (GTK_WINDOW (win));
}

static void on_addbutton_clicked (GtkWidget *widget, CoronabrowserAdditem *win)
{
  gtk_window_close (GTK_WINDOW (win));
}

static void coronabrowser_additem_class_init (CoronabrowserAdditemClass *class)
{
  G_OBJECT_CLASS (class)->dispose = coronabrowser_additem_dispose;
  gtk_widget_class_set_template_from_resource (GTK_WIDGET_CLASS (class), "/de/acwn/coronabrowser/additem.ui");

  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserAdditem, addbutton);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserAdditem, cancelbutton);
  gtk_widget_class_bind_template_child (GTK_WIDGET_CLASS (class), CoronabrowserAdditem, entryitem);

  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_cancelbutton_clicked);
  gtk_widget_class_bind_template_callback (GTK_WIDGET_CLASS (class), on_addbutton_clicked); 
}


CoronabrowserAdditem *coronabrowser_additem_new (CoronabrowserBookmarks *win)
{
  return g_object_new (CORONABROWSER_ADDITEM_TYPE, "transient-for", win, NULL);
}

const gchar *coronabrowser_additem_get_item (CoronabrowserAdditem *win)
{
  g_return_val_if_fail (g_utf8_strlen (gtk_entry_get_text (GTK_ENTRY (win->entryitem)), -1), "Untitled");
  return gtk_entry_get_text (GTK_ENTRY (win->entryitem));
}
