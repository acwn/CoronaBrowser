#!/bin/bash
set -e 

VERSION=""

while getopts ":v:h" opt; do
  case $opt in
    h)
      echo "Usage: $0 -v <version>"
      exit 0
    ;;
    v)
      VERSION=$OPTARG
    ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
    ;;
  esac
done

echo $VERSION
sed -e "s/__VERSION__/$VERSION/" configure.ac.in > configure.ac
autoreconf
exit 0
